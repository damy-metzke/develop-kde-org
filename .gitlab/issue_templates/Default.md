### Is this a website bug, an issue with the tutorial content, an issue with the Human Interface Guidelines (HIG), a question about the content, or something else?

- [ ] Website bug
- [ ] Tutorial content
- [ ] HIG content
- [ ] Question about content

(Something else)

### What pages are affected?

(Mention the name of the pages or paste their links here)

### Please describe the issue here:

(Explain the issue you are encountering here)

### Additional information

(Add screenshots, link to other relevant websites or issues, or suggest potential fixes here)
